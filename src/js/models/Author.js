var mODELcLASS = require('mODELcLASS');

module.exports = new mODELcLASS({
  name: 'Author',
  properties: {
    authorId: {
      label: 'ID',
      range: 'NonEmptyString',
      isStandardId: true,
      min: 1,
      max: 50
    },
    name: {
      label: 'Name',
      range: 'NonEmptyString',
      min: 2,
      max: 50
    }
  }
});