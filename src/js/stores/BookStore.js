var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var StatusConstants = require('../constants/StatusConstants');
var EventEmitter = require('events').EventEmitter;
var PublisherStore = require('../stores/PublisherStore');
var AuthorStore = require('../stores/AuthorStore');

var CHANGE_EVENT = 'change';
var books = [];

var BookStore = Object.assign({}, EventEmitter.prototype, {
  getBook: function getBook(id) {
    return books.filter(function (book) {
      return book.objectId === id;
    })[0];
  },

  getAllBooks: function () {
    return books;
  },

  emitChange: function () {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function (callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function (callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppDispatcher.register(function (action) {
  switch(action.type) {
    // -- Get all books
    case AppConstants.REQUEST_BOOKS_SUCCESS:
      try {
        books = action.result;
        BookStore.emitChange();
      } catch (e) {
        alert('Unvalid remote response format!');
      }
      break;

    // -- Create book Success
    case AppConstants.BOOK_SAVE_SUCCESS:
      try {
        var data = action.data;
        data.objectId = JSON.parse(action.result).objectId;
        data.publisher = data.publisher ?
                          PublisherStore.getPublisher(data.publisher.objectId) :
                          null;
        data.authors = data.authors && data.authors.objects && data.authors.objects.length ?
                        data.authors.objects.map(function (author) {
                          return AuthorStore.getAuthor(author.objectId);
                        }) : []

        books.push(data);
        BookStore.emitChange();
      } catch (e) {}
      break;

    // --- Update book Success
    case AppConstants.BOOK_UPDATE_SUCCESS:
      var data = action.data;

      books = books.map(function (book) {
        if (book.objectId === action.data.objectId) {
          book.title = data.title;
          book.year = data.year;
          book.publisher = data.publisher ?
                            PublisherStore.getPublisher(data.publisher.objectId) :
                            null;
          book.authors = data.authors && data.authors.objects && data.authors.objects.length ?
                          data.authors.objects.map(function (author) {
                            return AuthorStore.getAuthor(author.objectId);
                          }) : []
        }
        return book;
      });

      BookStore.emitChange();
      break;

    // -- Destroy book Success
    case AppConstants.BOOK_DESTROY_SUCCESS:
      books = books.filter(function (book) {
        return book.objectId !== action.data.objectId;
      });
      BookStore.emitChange();
      break;
  }
});

module.exports = BookStore;