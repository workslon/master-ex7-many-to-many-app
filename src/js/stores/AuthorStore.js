var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var StatusConstants = require('../constants/StatusConstants');
var BookActions = require('../actions/BookActions');
var BookModel = require('../models/Book');
var EventEmitter = require('events').EventEmitter;

var CHANGE_EVENT = 'change';
var authors = [];

var AuthorStore = Object.assign({}, EventEmitter.prototype, {
  getAuthor: function (id) {
    return authors.filter(function (author) {
      return author.objectId === id;
    })[0];
  },

  getAllAuthors: function (type) {
    return authors;
  },

  emitChange: function () {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function (callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function (callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppDispatcher.register(function (action) {
  switch(action.type) {
    // -- Get all authors
    case AppConstants.REQUEST_AUTHORS_SUCCESS:
      try {
        authors = action.result;
        AuthorStore.emitChange();
      } catch (e) {
        alert('Unvalid remote response format!');
      }
      break;

    // -- Create author Success
    case AppConstants.AUTHOR_SAVE_SUCCESS:
      try {
        action.data.objectId = JSON.parse(action.result).objectId;
        authors.push(action.data);
        AuthorStore.emitChange();
      } catch (e) {}
      break;

    // --- Update author Success
    case AppConstants.AUTHOR_UPDATE_SUCCESS:
      authors = authors.map(function (author) {
        if (author.objectId === action.data.objectId) {
          author.name = action.data.name;
        }
        return author;
      });

      setTimeout(function () {
        BookActions.getBooks(BookModel);
      }, 0);

      AuthorStore.emitChange();
      break;

    // -- Destroy author Success
    case AppConstants.AUTHOR_DESTROY_SUCCESS:
      authors = authors.filter(function (author) {
        return author.objectId !== action.data.objectId;
      });

      setTimeout(function () {
        BookActions.getBooks(BookModel);
      }, 0);

      AuthorStore.emitChange();
      break;
  }
});

module.exports = AuthorStore;