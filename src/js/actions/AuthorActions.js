var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var adapter = require('../storage-managers/ParseApiAdapter');

module.exports = {
  validate: function(modelClass, key, value) {
    var errors = {};
    errors[key] = modelClass.check(key, value).message;

    AppDispatcher.dispatch({
      type: AppConstants.AUTHOR_VALIDATION_ERROR,
      errors: errors
    });
  },

  getAuthors: function (modelClass) {
    var promise = adapter.retrieveAll(modelClass);

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_AUTHORS,
      success: AppConstants.REQUEST_AUTHORS_SUCCESS,
      failure: AppConstants.REQUEST_AUTHORS_ERROR
    });
  },

  createAuthor: function (modelClass, data) {
    adapter
      .retrieve(modelClass, '', data.email)
      .then(function(records) {
        try {
          if (records.length) {
            AppDispatcher.dispatch({
              type: AppConstants.NON_UNIQUE_ID
            });
          } else {
            data.runCloudCode = true;
            AppDispatcher.dispatchAsync(adapter.add(modelClass, data), {
              request: AppConstants.REQUEST_AUTHOR_SAVE,
              success: AppConstants.AUTHOR_SAVE_SUCCESS,
              failure: AppConstants.AUTHOR_SAVE_ERROR
            }, data);
          }
        } catch(e) {}
      });

    AppDispatcher.dispatch({
      type: AppConstants.REQUEST_AUTHOR_SAVE
    });
  },

  updateAuthor: function (modelClass, author, newData) {
    var promise;

    newData.runCloudCode = true;
    promise = adapter.update(modelClass, author, newData);
    newData.objectId = author.objectId;

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_AUTHOR_UPDATE,
      success: AppConstants.AUTHOR_UPDATE_SUCCESS,
      failure: AppConstants.AUTHOR_UPDATE_ERROR
    }, newData);
  },

  deleteAuthor: function (modelClass, author) {
    var promise;

    author.runCloudCode = true;
    promise = adapter.destroy(modelClass, author);

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_AUTHOR_DESTROY,
      success: AppConstants.AUTHOR_DESTROY_SUCCESS,
      failure: AppConstants.AUTHOR_DESTROY_ERROR
    }, author);
  }
};